import VuePluginHamburgerBtn from "./VuePluginHamburgerBtn.vue";

export default {
  install(Vue, options) {
    Vue.component("vue-plugin-hamburger-btn", VuePluginHamburgerBtn);
  }
};
