## Hamburger Button Vue Component

#### Install
Add to your package.json in dependencies
``` bash
"vue-plugin-hamburger-btn": "git+ssh://git@bitbucket.org/digitalholding/vue-plugin-hamburger-btn.git#{version}"
```
Install packages
``` bash
npm install
```

Include in project globally as plugin
``` bash
import Vue from 'vue';
import VuePluginHamburgerBtn from 'vue-plugin-hamburger-btn';

Vue.use(VuePluginHamburgerBtn);
```

Usage
``` bash
<vue-plugin-hamburger-btn :toggle-menu-method="{ method }"
                          :open="{ variable }"
                           custom-color="{ hex color }"
                           custom-active-color="{ hex color }"
/>
```

#### Building & editing:
You can rapidly prototype with just a single *.vue file with the vue serve and vue buildcommands, 
but they require an additional global addon to be installed first:
``` bash
npm install -g @vue/cli
npm install -g @vue/cli-service-global
```

Install packages
``` bash
npm install
```

Serve on localhost command  
``` bash
vue serve src/VuePluginHamburgerBtn.vue
```

Bundling Your Library (need devDependencies: bili, rollup-plugin-vue, vue-template-compiler, node-sass, sass-loader)
``` bash
npx bili --bundle-node-modules
```


#### Component props
Method for toggling menu
``` bash
toggleMenuMethod: {
  type: Function,
  required: true,
  default: () => console.log('Missing toggle menu method.')
},
```

Variable define that menu is open/close
``` bash
open: {
  type: Boolean,
  required: true,
  default: false
},
```

Icon color when menu is close
``` bash
customColor: {
  type: String,
  default: '#053952'
},
```

Icon color when menu is open
``` bash
customActiveColor: {
  type: String,
  default: '#1fae52'
}
```
