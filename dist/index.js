/*!
 * vue-plugin-hamburger-btn v0.0.1
 * (c) Dominik Łyczko
 * Released under the MIT License.
 */
'use strict';

//
//
//
//
//
//
//
//
var script = {
  name: "VuePluginHamburgerBtn",
  props: {
    toggleMenuMethod: {
      type: Function,
      required: true,
      "default": function _default() {
        return console.log('Missing toggle menu method.');
      }
    },
    open: {
      type: Boolean,
      required: true,
      "default": false
    },
    customColor: {
      type: String,
      "default": '#053952'
    },
    customActiveColor: {
      type: String,
      "default": '#1fae52'
    }
  },
  computed: {
    backgroundColor: function backgroundColor() {
      return this.open ? this.customActiveColor : this.customColor;
    }
  }
};

function normalizeComponent(template, style, script, scopeId, isFunctionalTemplate, moduleIdentifier /* server only */, shadowMode, createInjector, createInjectorSSR, createInjectorShadow) {
    if (typeof shadowMode !== 'boolean') {
        createInjectorSSR = createInjector;
        createInjector = shadowMode;
        shadowMode = false;
    }
    // Vue.extend constructor export interop.
    const options = typeof script === 'function' ? script.options : script;
    // render functions
    if (template && template.render) {
        options.render = template.render;
        options.staticRenderFns = template.staticRenderFns;
        options._compiled = true;
        // functional template
        if (isFunctionalTemplate) {
            options.functional = true;
        }
    }
    // scopedId
    if (scopeId) {
        options._scopeId = scopeId;
    }
    let hook;
    if (moduleIdentifier) {
        // server build
        hook = function (context) {
            // 2.3 injection
            context =
                context || // cached call
                    (this.$vnode && this.$vnode.ssrContext) || // stateful
                    (this.parent && this.parent.$vnode && this.parent.$vnode.ssrContext); // functional
            // 2.2 with runInNewContext: true
            if (!context && typeof __VUE_SSR_CONTEXT__ !== 'undefined') {
                context = __VUE_SSR_CONTEXT__;
            }
            // inject component styles
            if (style) {
                style.call(this, createInjectorSSR(context));
            }
            // register component module identifier for async chunk inference
            if (context && context._registeredComponents) {
                context._registeredComponents.add(moduleIdentifier);
            }
        };
        // used by ssr in case component is cached and beforeCreate
        // never gets called
        options._ssrRegister = hook;
    }
    else if (style) {
        hook = shadowMode
            ? function (context) {
                style.call(this, createInjectorShadow(context, this.$root.$options.shadowRoot));
            }
            : function (context) {
                style.call(this, createInjector(context));
            };
    }
    if (hook) {
        if (options.functional) {
            // register for functional component in vue file
            const originalRender = options.render;
            options.render = function renderWithStyleInjection(h, context) {
                hook.call(context);
                return originalRender(h, context);
            };
        }
        else {
            // inject component registration as beforeCreate hook
            const existing = options.beforeCreate;
            options.beforeCreate = existing ? [].concat(existing, hook) : [hook];
        }
    }
    return script;
}

const isOldIE = typeof navigator !== 'undefined' &&
    /msie [6-9]\\b/.test(navigator.userAgent.toLowerCase());
function createInjector(context) {
    return (id, style) => addStyle(id, style);
}
let HEAD;
const styles = {};
function addStyle(id, css) {
    const group = isOldIE ? css.media || 'default' : id;
    const style = styles[group] || (styles[group] = { ids: new Set(), styles: [] });
    if (!style.ids.has(id)) {
        style.ids.add(id);
        let code = css.source;
        if (css.map) {
            // https://developer.chrome.com/devtools/docs/javascript-debugging
            // this makes source maps inside style tags work properly in Chrome
            code += '\n/*# sourceURL=' + css.map.sources[0] + ' */';
            // http://stackoverflow.com/a/26603875
            code +=
                '\n/*# sourceMappingURL=data:application/json;base64,' +
                    btoa(unescape(encodeURIComponent(JSON.stringify(css.map)))) +
                    ' */';
        }
        if (!style.element) {
            style.element = document.createElement('style');
            style.element.type = 'text/css';
            if (css.media)
                style.element.setAttribute('media', css.media);
            if (HEAD === undefined) {
                HEAD = document.head || document.getElementsByTagName('head')[0];
            }
            HEAD.appendChild(style.element);
        }
        if ('styleSheet' in style.element) {
            style.styles.push(code);
            style.element.styleSheet.cssText = style.styles
                .filter(Boolean)
                .join('\n');
        }
        else {
            const index = style.ids.size - 1;
            const textNode = document.createTextNode(code);
            const nodes = style.element.childNodes;
            if (nodes[index])
                style.element.removeChild(nodes[index]);
            if (nodes.length)
                style.element.insertBefore(textNode, nodes[index]);
            else
                style.element.appendChild(textNode);
        }
    }
}

/* script */
var __vue_script__ = script;
/* template */

var __vue_render__ = function __vue_render__() {
  var _vm = this;

  var _h = _vm.$createElement;

  var _c = _vm._self._c || _h;

  return _c('div', {
    staticClass: "vue-plugin-hamburger-btn",
    "class": {
      open: _vm.open
    },
    on: {
      "click": _vm.toggleMenuMethod
    }
  }, [_c('span', {
    style: {
      background: _vm.backgroundColor
    }
  }), _vm._v(" "), _c('span', {
    style: {
      background: _vm.backgroundColor
    }
  }), _vm._v(" "), _c('span', {
    style: {
      background: _vm.backgroundColor
    }
  })]);
};

var __vue_staticRenderFns__ = [];
/* style */

var __vue_inject_styles__ = function __vue_inject_styles__(inject) {
  if (!inject) return;
  inject("data-v-748b0b27_0", {
    source: ".vue-plugin-hamburger-btn[data-v-748b0b27]{display:none;width:30px;height:22px;position:relative;margin:10px;transform:rotate(0);transition:.5s ease-in-out;cursor:pointer}.vue-plugin-hamburger-btn span[data-v-748b0b27]{display:block;position:absolute;height:3px;width:100%;border-radius:4px;opacity:1;left:0;transform:rotate(0);transition:.25s ease-in-out}.vue-plugin-hamburger-btn span[data-v-748b0b27]:nth-child(1){top:0}.vue-plugin-hamburger-btn span[data-v-748b0b27]:nth-child(2){top:9px}.vue-plugin-hamburger-btn span[data-v-748b0b27]:nth-child(3){top:18px}.vue-plugin-hamburger-btn.open span[data-v-748b0b27]:nth-child(1){top:9px;transform:rotate(135deg)}.vue-plugin-hamburger-btn.open span[data-v-748b0b27]:nth-child(2){opacity:0;left:-30px}.vue-plugin-hamburger-btn.open span[data-v-748b0b27]:nth-child(3){top:9px;transform:rotate(-135deg)}@media (max-width:750px){.vue-plugin-hamburger-btn[data-v-748b0b27]{display:inline-block}}@media (max-width:400px){.vue-plugin-hamburger-btn[data-v-748b0b27]{width:24px;height:17px;margin:10px}.vue-plugin-hamburger-btn span[data-v-748b0b27]:nth-child(1){top:0}.vue-plugin-hamburger-btn span[data-v-748b0b27]:nth-child(2){top:7px}.vue-plugin-hamburger-btn span[data-v-748b0b27]:nth-child(3){top:14px}}",
    map: undefined,
    media: undefined
  });
};
/* scoped */


var __vue_scope_id__ = "data-v-748b0b27";
/* module identifier */

var __vue_module_identifier__ = undefined;
/* functional template */

var __vue_is_functional_template__ = false;
/* style inject SSR */

/* style inject shadow dom */

var __vue_component__ = /*#__PURE__*/normalizeComponent({
  render: __vue_render__,
  staticRenderFns: __vue_staticRenderFns__
}, __vue_inject_styles__, __vue_script__, __vue_scope_id__, __vue_is_functional_template__, __vue_module_identifier__, false, createInjector, undefined, undefined);

var index = {
  install: function install(Vue, options) {
    Vue.component("vue-plugin-hamburger-btn", __vue_component__);
  }
};

module.exports = index;
